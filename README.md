# dwm-cvc

My own build of DWM with the following patches already applied (see folder patches):

- Hide vacant tags: hide the unused tags.
- Move resize: move and resize float windows in DWM.
- Per tag: saves and applies individually each tag configuration.
- statuscolors and systray: it merges those two widely used patches, allowing add colour to the status bar, showing icons from the systry. This patch eliminates interferences and inconsistencies produced when the user try to do both patches separately.

# Installation

Please follow the instructions specified in the README file.
